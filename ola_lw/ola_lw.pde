import java.util.Random;
import java.io.FileNotFoundException;

final String API_ROOT = "http://ola.wrbl606.ct8.pl";

int currentBackground = 0, currentImageIndex = 0;
long lastUpdateTime = 0, lastNetworkConnectionTime = 0;
PImage currentImage;
Random random = new Random();

String backgrounds[] = {
  "FFf48fb1", "FFb39ddb", "FF9fa8da", "FF90caf9",
  "FF81d4fa", "FF80deea", "FF80cbc4", "FFa5d6a7",
  "FFc5e1a5", "FFe6ee9c", "FFffe082", "FFffcc80",
  "FFffab91", "FFbcaaa4", "FFb0bec5"
};

void setup() {
  orientation(PORTRAIT);
  fullScreen();
  colorMode(RGB, 100);
  background(unhex(backgrounds[currentBackground]));
  lookForNewImages();
  lastNetworkConnectionTime = millis();
  currentImage = getNextImageToDraw();
}

boolean firstImageDrawn = false;

void draw() {
  if (!firstImageDrawn && currentImage != null) {
    drawImage(currentImage);
    firstImageDrawn = true;
  }
  
  if(millis() - lastUpdateTime > 30000) {
    lastUpdateTime = millis();
    System.out.println("UPDATE");
    currentImage = getNextImageToDraw();
    currentBackground = getNextBackground();
    background(unhex(backgrounds[currentBackground]));
    drawImage(currentImage);
  }
  
  if(millis() - lastNetworkConnectionTime > 600000) {
    lastNetworkConnectionTime = millis();
    lookForNewImages();
  }
}

PImage getNextImageToDraw() {
  JSONArray knownImages = getKnownImagesList();
  int imagesAvailable = knownImages.size();
  
  if (imagesAvailable > 1) {
      int randomIndex = random.nextInt(imagesAvailable);
     
      do {
        randomIndex = random.nextInt(imagesAvailable);
      } while (randomIndex == currentImageIndex);
      currentImageIndex = randomIndex;
  } else if (imagesAvailable == 0) {
    drawLoadingText();
    return null;
  }
  
  PImage imageToDraw = loadImageFromFile(knownImages.getString(currentImageIndex));
  return imageToDraw;
}

int getNextBackground() {
  return random.nextInt(this.backgrounds.length);
}

void drawLoadingText() {
  textSize(72);
  text(
    "Jeszcze sekundka...",
    (width/2) - 320,
    (height/2) - 46);
}

void drawImage(PImage imageToDraw) {
  if (imageToDraw == null) return;
  
  // image should be always sqare (1080x1080)
  int imageSize = 1080;
  
  int newWidth = (width < height) ? width : height;
  int newHeight = (height > width) ? height : width;
  
  image(
    imageToDraw,
    (newWidth - imageSize)/2,
    (newHeight - imageSize)/2,
    1080,
    1080
  );
}

void saveImage(PImage image, String name) {
  System.out.println("Saved new image: " + name);
  image.save(name);
  addImageNameToKnownList(name);
}

PImage loadImageFromFile(String filename) {
  return loadImage(filename);
}

void addImageNameToKnownList(String filename) {
  JSONArray list = getKnownImagesList();
  
  list.append(filename);
  saveStrings("images.list", new String[] {list.toString()});
  
}

JSONArray getKnownImagesList() {
  String strings[] = loadStrings("images.list");

  if (strings == null) {
    saveStrings("images.list", new String[] {"[]"});
  }

  if (strings == null) {
    strings = new String[] {"[]"};
  }

  String completeFile = "";
  for (String line : strings) {
    completeFile += line;
  }
  
  JSONArray imagesArray = JSONArray.parse(completeFile);
  return imagesArray;
}

JSONArray getImagesAvailableOnRemote() {
  String strings[] = loadStrings(API_ROOT + "/images.list");

  if (strings == null) {
    strings = new String[] {"[]"};
  }

  String completeFile = "";
  for (String line : strings) {
    completeFile += line;
  }
  
  JSONArray imagesArray = JSONArray.parse(completeFile);
  return imagesArray;
}

void lookForNewImages() {
  System.out.println("lookForNewImages"); 
  
  JSONArray downloadedImagesList = getKnownImagesList();
  JSONArray remoteImagesList = getImagesAvailableOnRemote();
  ArrayList<String> notOnLocal = new ArrayList();
  
  
  if (downloadedImagesList.size() != 0) {
    for (int i = 0; i < remoteImagesList.size(); i++) {
      String currentImage = remoteImagesList.getString(i);
      boolean isDownloaded = false;
      for (int j = 0; j < downloadedImagesList.size(); j++) {
        if (currentImage.equals(downloadedImagesList.getString(j))) isDownloaded = true;
      }
      
      if (!isDownloaded) notOnLocal.add(currentImage);
    }
  } else {
    for (int j = 0; j < remoteImagesList.size(); j++) {
      notOnLocal.add(remoteImagesList.getString(j));
    }
  }

  
  for (String newImage : notOnLocal) {
    System.out.println("Downloading from remote: " + newImage);
    saveImage(loadImage(API_ROOT + "/image/" + newImage), newImage);  
  }
}
